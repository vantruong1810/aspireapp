# Installations
- Clone gitlab repository and install dependency packages:
```bash
git clone https://gitlab.com/vantruong1810/aspireapp.git
cd aspireapp
composer install --optimize-autoloader --no-dev # or install full dev dependencies `composer install`
cp .env.example .env
php artisan key:generate
```
- Open `.env` and correct database connection:

For example:
```dotenv
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=root
DB_PASSWORD=
```
- Start Laravel Aspireapp
```shell
php artisan serve
```
# Reference:
- Demo link: https://aspire.trcorner.com
- Postman: https://www.postman.com/sfs-api/workspace/public-workspace/collection/498848-754d1d57-6863-41a6-bc7c-638116c2d331
- Official Laravel documentation: https://laravel.com/docs
