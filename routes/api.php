<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\LoanController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Authentication
Route::prefix('auth')->group(function () {
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/login', [AuthController::class, 'login']);
    Route::middleware('auth:sanctum')->post('/logout', [AuthController::class, 'logout']);
});

// Loan
Route::group(['prefix' => 'loan', 'middleware' => 'auth:sanctum'], function() {
    // Create a new loan
    Route::post('/', [LoanController::class, 'create']);
    // Approve the loan (for quick test)
    Route::post('/{id}/approve', [LoanController::class, 'approve']);
    // Loan repayment
    Route::post('/{id}/repayment', [LoanController::class, 'repayment']);
});
