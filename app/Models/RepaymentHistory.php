<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RepaymentHistory extends Model
{
    use HasFactory;
    protected $fillable = [
      'id',
      'loan_id',
      'amount',
      'note'
    ];
    protected $keyType = 'string';
    public $incrementing = false;

    /**
     * Get loan info
     */
    public function loan() {
        return $this->belongsTo(Loan::class);
    }
}
