<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'user_id',
        'amount',
        'repayment_amount',
        'loan_term',
        'note',
        'approved'
    ];
    protected $keyType = 'string';
    public $incrementing = false;

    /**
     * Get user that owns the loan
     */
    public function user() {
        return $this->belongsTo(User::class);
    }

    /**
     * Get repayment history for the loan
     */
    public function repayment_histories() {
        return $this->hasMany(RepaymentHistory::class);
    }
}
