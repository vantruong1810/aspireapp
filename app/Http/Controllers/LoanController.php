<?php

namespace App\Http\Controllers;

use App\Models\RepaymentHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Models\Loan;
use App\Models\User;

class LoanController extends Controller
{
    /**
     * Create a new loan
     */
    public function create(Request $request)
    {
        $fields = $request->validate([
            'amount' => 'required|numeric',
            'loan_term' => 'required|integer|min:1',
            'note' => 'string'
        ]);

        $loan = Loan::create([
            'id' => Str::uuid()->toString(),
            'user_id' => $request->user()->id,
            'amount' => $fields['amount'],
            'repayment_amount' => $fields['amount'],
            'loan_term' => $fields['loan_term'],
            'note' => $fields['note'] ?? null
        ]);

        return response($loan, 201);
    }

    /**
     * [For testing only] Approve the loan by himself
     */
    public function approve(Request $request, $id)
    {
        // Check existing the loan for current user
        $loan = User::find($request->user()->id)->loans()->where('id', $id)->first();
        if (!$loan) {
            return response([
                'message' => 'The selected loan is invalid'
            ], 422);
        }
        $loan->approved = true;
        $loan->save();

        return response($loan, 200);
    }

    /**
     * Repayment the loan
     */
    public function repayment(Request $request, $id)
    {
        $loan = User::find($request->user()->id)->loans()->where('id', $id)->first();
        if (!$loan) {
            // The loan was not found
            return response([
                'message' => 'The selected loan is invalid'
            ], 422);
        } elseif (!$loan->approved) {
            // The loan was not approved
            return response([
                'message' => 'The selected loan was not approved'
            ], 200);
        } elseif ($loan->repayment_amount <= 0) {
            // The loan was completed
            return response([
                'message' => 'The selected loan was completed'
            ], 200);
        }

        $loan_end_date = Carbon::create($loan->created_at)->addDays($loan->loan_term);
        $now = Carbon::now();
        if ($loan_end_date->lt($now)) {
            // The loan was expired
            return response([
                'message' => 'The loan term was expired',
                'loan' => $loan
            ], 200);
        }

        // Calculate repayment amount by week
        $amount_per_week = round($loan->amount * 7 / $loan->loan_term, 2);
        $amount = min($loan->repayment_amount, $amount_per_week);

        // Update remaining amount
        $loan->repayment_amount = round($loan->repayment_amount - $amount, 2);
        $loan->save();

        // Add repayment history
        $history = RepaymentHistory::create([
            'id' => Str::uuid()->toString(),
            'loan_id' => $loan->id,
            'amount' => $amount,
            'note' => $fields['note'] ?? null
        ]);

        return response([
            'message' => 'Repayment was accepted',
            'repayment_info' => $history,
            'loan' => $loan
        ], 201);
    }
}
