<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LoanTest extends TestCase
{
    protected $prefix = 'api/loan/';

    /**
     * Create Loan feature test.
     *
     * @return void
     */
    public function test_create_loan_unauthenticated()
    {
        $response = $this->postJson($this->prefix, [
            "amount" => 100.20,
            "loan_term" => 12,
            "note" => "Note"
        ]);

        $response->assertUnauthorized();
    }

    /**
     * Create Loan without request data.
     *
     * @return void
     */
    public function test_create_loan_without_mandatory_fields()
    {
        $user = User::factory()->create();
        $response = $this->actingAs($user)->postJson($this->prefix);

        $response->assertUnprocessable();
    }

    /**
     * Create Loan request data successfully.
     *
     * @return void
     */
    public function test_create_loan()
    {
        $user = User::factory()->create();
        $data = [
            "amount" => 100.20,
            "loan_term" => 12,
            "note" => "Note"
        ];
        $response = $this->actingAs($user)->postJson($this->prefix, $data);

        $response->assertStatus(201);
        $response->assertJson($data);
    }

    /**
     * Create Loan feature test.
     *
     * @return void
     */
    public function test_approve_loan_unauthenticated()
    {
        $response = $this->postJson($this->prefix . 'f98facc7-b3a1-462e-b9a8-a22179f95d74/approve');

        $response->assertUnauthorized();
    }

    /**
     * Approve loan
     *
     * @return void
     */
    public function test_approve_loan()
    {
        $user = User::factory()->create();
        $data = [
            "amount" => 100.20,
            "loan_term" => 12,
            "note" => "Note"
        ];
        $loan = $this->actingAs($user)->postJson($this->prefix, $data);

        $response = $this->actingAs($user)->postJson($this->prefix . $loan['id'] . '/approve');

        $response->assertStatus(200);
        $response->assertJson($data);
        $response->assertSee('approved');
    }

    /**
     * Repayment loan
     *
     * @return void
     */
    public function test_repayment_loan_unauthenticated()
    {
        $response = $this->postJson($this->prefix . 'f98facc7-b3a1-462e-b9a8-a22179f95d74/repayment');

        $response->assertUnauthorized();
    }

    /**
     * Repayment loan
     *
     * @return void
     */
    public function test_repayment_loan()
    {
        $user = User::factory()->create();
        $data = [
            "amount" => 100.20,
            "loan_term" => 12, // 2 weeks for payment
            "note" => "Note"
        ];
        // Create a new loan
        $loan = $this->actingAs($user)->postJson($this->prefix, $data);
        // Approve the loan
        $this->actingAs($user)->postJson($this->prefix . $loan['id'] . '/approve');
        // Accept 1st payment
        $response1 = $this->postJson($this->prefix . $loan['id'] . '/repayment');
        $response1->assertStatus(201);
        $response1->assertJsonStructure([
            'message',
            'repayment_info',
            'loan'
        ]);
        // Accept 2nd payment
        $response2 = $this->postJson($this->prefix . $loan['id'] . '/repayment');
        $response2->assertStatus(201);
        $response2->assertJsonStructure([
            'message',
            'repayment_info',
            'loan'
        ]);
        // Just show completed message
        $response3 = $this->postJson($this->prefix . $loan['id'] . '/repayment');
        $response3->assertStatus(200);
        $response3->assertJsonStructure([
            'message'
        ]);
    }
}
